var require = meteorInstall({"client":{"main.html":function(require,exports,module){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// client/main.html                                                  //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
module.exports = require("./template.main.js");                      // 1
                                                                     // 2
///////////////////////////////////////////////////////////////////////

},"template.main.js":function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// client/template.main.js                                           //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
                                                                     // 1
Template.body.addContent((function() {                               // 2
  var view = this;                                                   // 3
  return HTML.Raw('<header id="main-header">\n    \n    <a id="logo-header" href="#">\n      <h1>Rally de matematicas</h1>\n    </a> <!-- / #logo-header -->\n \n    <nav>\n      <ul>\n        <li><a href="#">administrar</a></li>\n      </ul>\n    </nav><!-- / nav -->\n \n  </header><!-- / #main-header -->\n \n  \n  <section id="main-content">\n  \n    <article>\n      <header>\n        <h2>Esta es una página con cabecera fija</h2>\n      </header>\n      \n      <div class="container">\n\n        <div class="w3-row-padding">\n  <div class="w3-third">\n    <h2>Bases</h2>\n    <a href="#"> <img src="images/registro.png" width="100" height="100" align="center"></a>\n  </div>\n\n  <div class="w3-third">\n    <h2>Registro</h2>\n    <a href="#"> <img src="images/registro.png" width="100" height="100" align="center"></a>\n  </div>\n\n  <div class="w3-third">\n    <h2>Ganadores</h2>\n  <a href="bases.html"><img width="100" height="100" align="center" src="images/ganador.png"></a>\n  </div>\n</div>\n       \n      </div>\n      \n    </article> <!-- /article -->\n  \n  </section> <!-- / #main-content -->\n \n  \n  \n  <footer id="main-footer">\n    <p>&copy; 2017 Instituto Tecnológico de Veracruz</p>\n  </footer> <!-- / #main-footer -->');
}));                                                                 // 5
Meteor.startup(Template.body.renderToDocument);                      // 6
                                                                     // 7
///////////////////////////////////////////////////////////////////////

},"main.js":function(require,exports,module){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// client/main.js                                                    //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
var Template = void 0;                                               // 1
module.watch(require("meteor/templating"), {                         // 1
  Template: function (v) {                                           // 1
    Template = v;                                                    // 1
  }                                                                  // 1
}, 0);                                                               // 1
var ReactiveVar = void 0;                                            // 1
module.watch(require("meteor/reactive-var"), {                       // 1
  ReactiveVar: function (v) {                                        // 1
    ReactiveVar = v;                                                 // 1
  }                                                                  // 1
}, 1);                                                               // 1
module.watch(require("../imports/startup/accounts-config.js"));      // 1
module.watch(require("../imports/ui/body.js"));                      // 1
module.watch(require("./main.html"));                                // 1
Template.hello.onCreated(function () {                               // 8
  function helloOnCreated() {                                        // 8
    // counter starts at 0                                           // 9
    this.counter = new ReactiveVar(0);                               // 10
  }                                                                  // 11
                                                                     //
  return helloOnCreated;                                             // 8
}());                                                                // 8
Template.hello.helpers({                                             // 13
  counter: function () {                                             // 14
    return Template.instance().counter.get();                        // 15
  }                                                                  // 16
});                                                                  // 13
Template.hello.events({                                              // 19
  'click button': function (event, instance) {                       // 20
    // increment the counter when button is clicked                  // 21
    instance.counter.set(instance.counter.get() + 1);                // 22
  }                                                                  // 23
});                                                                  // 19
///////////////////////////////////////////////////////////////////////

}}},{
  "extensions": [
    ".js",
    ".json",
    ".html",
    ".css"
  ]
});
require("./client/template.main.js");
require("./client/main.js");